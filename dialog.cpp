#include "dialog.h"
#include "ui_dialog.h"
#include "ftd2xx.h"
#include <QtConcurrentRun>
#include <QtGlobal>
#include "ftctrlworker.h"
#include <QPalette>
#include <QColor>
#include <QFileDialog>

#include "dataTypes.h"

#include <QQueue>
//#include <QFuture>
//#include <QFutureWatcher>

#include <QTextEdit>

#include <QTimer>

extern QMutex sendQueueMutex;
extern QQueue<QVarLengthArray<quint8>> sendQueue;

Dialog::Dialog(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::Dialog)
    //, ftCtrlThread(this)
    , ftHandle(nullptr)
    , worker(nullptr)
    , settings ("NIIP", "NIIP-service")
    , msgs(0)
    , eDlThresh (1000)
{
    ui->setupUi(this);

    QObject::connect(ui->pushButtonCaruOn, &QPushButton::clicked, [=](){sendReq(0x81);});

    bool bAutoUprCheckState = settings.value("auto_upr").toBool();
    ui->checkBoxAutoUpr->setCheckState(bAutoUprCheckState?Qt::Checked:Qt::Unchecked);

    bool bAutoECheckState = settings.value("auto_e").toBool();
    ui->checkBoxAutoEBus->setCheckState(bAutoECheckState?Qt::Checked:Qt::Unchecked);

    QLayout *l = layout();  // .w QVBoxLayout(&w);
    l->addWidget(new QTextEdit);
    statusBar = new QStatusBar;
    l->addWidget(statusBar);
    statusBar->showMessage("XXX", 5000);
    lineEditMsgCounter = new QLineEdit("n/a", statusBar);
    lineEditMsgCounter->setMaximumWidth(80);
    lineEditMsgCounter->setReadOnly(true);
    lineEditMsgCounter->setEnabled(false);
    lineEditMsgCounter->setAlignment(Qt::AlignHCenter);
    statusBar->addPermanentWidget(new QLabel("msgs/sec:"));
    statusBar->addPermanentWidget(lineEditMsgCounter);

    lineEditOnlineCouter= new QLineEdit("n/a", statusBar);
    lineEditOnlineCouter->setMaximumWidth(50);
    lineEditOnlineCouter->setReadOnly(true);
    lineEditOnlineCouter->setEnabled(false);
    lineEditOnlineCouter->setAlignment(Qt::AlignHCenter);
    statusBar->addPermanentWidget(new QLabel("connected:"));
    statusBar->addPermanentWidget(lineEditOnlineCouter);

    l->setMargin(0);
    l->setSpacing(0);

    QTimer *timer = new QTimer();
    connect(timer, &QTimer::timeout, [=](){updateMsgCounter();});
    timer->start(100);

    ui->tabWidgetIniListTabs->clear();
    ui->tabWidgetIniListTabs->setTabsClosable(true);
    //connect(ui->tabWidgetIniListTabs, &QTabWidget::tabCloseRequested, [=](int a){qDebug("req close %d", a);});
    connect(ui->tabWidgetIniListTabs, SIGNAL(tabCloseRequested(int)), SLOT(closeTabReq(int)));

    QString openedFileNew, openedIniFiles = settings.value("openedIniFiles", "").toString();
    //openedIniFiles.remove(filePath);
    QStringList openedFileList = openedIniFiles.split(";");

    foreach(QString filePath, openedFileList){
        openIniFile(filePath);
    }

    connect(ui->pushButtonReqEBus0, &QPushButton::clicked, [=](int a){sendReq(0xe0);});
    connect(ui->pushButtonReqEBus1, &QPushButton::clicked, [=](int a){sendReq(0xe1);});
    connect(ui->pushButtonReqQBus, &QPushButton::clicked, [=](int a){sendReq(0xdd);});
    connect(ui->pushButtonReqParamsSend, &QPushButton::clicked, [=](int a){sendReq(0xde);});
    connect(ui->checkBoxSendParamsOnTim2, &QPushButton::clicked, [=](bool e){sendReq(e?0xda:0xdb);});
    connect(ui->checkBoxEbusTestMode, &QPushButton::clicked, [=](bool e){sendReq(e?0xea:0xeb);});


    QString ipRange = "(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])";
    /* Создаем регулярное выражение с применением строки, как
     * повторяющегося элемента
     */
    QRegExp ipRegex ("^" + ipRange
                     + "\\." + ipRange
                     + "\\." + ipRange
                     + "\\." + ipRange + "$");
    /* Создаем Валидатор регулярного выражения с применением
     * созданного регулярного выражения
     */
    QRegExpValidator *ipValidator = new QRegExpValidator(ipRegex, this);
    /* Устанавливаем Валидатор на QLineEdit */
    ui->lineEditUdpIP->setValidator(ipValidator);

    QString udpAddr = settings.value("udpAddr").toString();
    if(udpAddr.isEmpty())
        udpAddr = "127.0.0.1";
    ui->lineEditUdpIP->setText(udpAddr);

    bool bSendUdp = settings.value("sendUdp").toBool();
    ui->checkBoxUDPSend->setChecked(bSendUdp);

    ui->checkBoxEBusFileSave->setChecked(settings.value("saveToFile", false).toBool());
}


Dialog::~Dialog()
{
    if(worker != NULL){
        worker->cancel();
        ftCtrlThread.quit();
        ftCtrlThread.wait();
        //delete worker;
    }

    delete ui;
}

//class FtCtrlWorker : public QObject
//{
//    Q_OBJECT

//public slots:
//    void doWork(const QString &parameter) {
//        QString result;
//        /* ... here is the expensive or blocking operation ... */
//        emit resultReady(result);
//        while(true){

//            qDebug("ping");
//            QThread::sleep(500);
//        }
//    }

//signals:
//    void resultReady(const QString &result);
//};




void Dialog::on_pushButtonOpenCloseUsb_clicked(bool checked)
{
    if(ui->pushButtonOpenCloseUsb->text() == "open device"){
        FT_STATUS ftStatus;
        ftStatus = FT_Open(0, &ftHandle);
        qDebug("FT_Open %d", (uint32_t)ftStatus);

        if(ftStatus != FT_OK){
            QString errStr;
            errStr = QString().sprintf("open error %x", ftStatus);
            switch(ftStatus){
                case FT_INVALID_HANDLE: errStr += QString(": \"FT_INVALID_HANDLE\""); break;
                case FT_DEVICE_NOT_FOUND: errStr += QString(": \"FT_DEVICE_NOT_FOUND\""); break;
                case FT_DEVICE_NOT_OPENED: errStr += QString(": \"FT_DEVICE_NOT_OPENED\""); break;
                case FT_IO_ERROR: errStr += QString(": \"FT_IO_ERROR\""); break;
                case FT_INSUFFICIENT_RESOURCES: errStr += QString(": \"FT_INSUFFICIENT_RESOURCES\""); break;
                case FT_INVALID_PARAMETER: errStr += QString(": \"FT_INVALID_PARAMETER\""); break;
                case FT_INVALID_BAUD_RATE: errStr += QString(": \"FT_INVALID_BAUD_RATE\""); break;
                case FT_NOT_SUPPORTED: errStr += QString(": \"FT_NOT_SUPPORTED\""); break;
                case FT_DEVICE_LIST_NOT_READY: errStr += QString(": \"FT_DEVICE_LIST_NOT_READY\""); break;
            }

            statusBar->showMessage(errStr, 2500);
            return;
        }
        ftStatus = FT_SetTimeouts(ftHandle, 1000, 1000);
        qDebug("FT_SetTimeouts %d", (uint32_t)ftStatus);
        ftStatus = FT_SetBitMode(ftHandle, 0xff, 0x00);
        qDebug("FT_SetBitMode %d", (uint32_t)ftStatus);
        ftStatus = FT_SetBitMode(ftHandle, 0xff, 0x40);
        qDebug("FT_SetBitMode %d", (uint32_t)ftStatus);
        //QFuture<void> future = QtConcurrent::run(ftRecvThread, ftHandle);
        ui->pushButtonOpenCloseUsb->setText("close device");

        //QFutureWatcher<void> watcher;
        //QObject::connect(&watcher, SIGNAL(finished()), SLOT(quit()));
        //QObject::connect(&watcher, SIGNAL(progressValueChanged(int)), &progress, SLOT(setValue(int)));
        //watcher.setFuture(future);

        worker = new FtCtrlWorker();
        worker->clearMsgCount();
        worker->setHandle(ftHandle);

        worker->moveToThread(&ftCtrlThread);
        connect(&ftCtrlThread, &QThread::finished, worker, &QObject::deleteLater);
        //connect(this, &Dialog::operate, worker, &FtCtrlWorker::doWork);
        connect(&ftCtrlThread, SIGNAL(started()), worker, SLOT(process()));
        //connect(worker, &Worker::resultReady, this, &Controller::handleResults);
        //ftCtrlThread.start();

        connect(worker, SIGNAL(uiUpdate(uint8_t*, quint32)), this, SLOT(uiUpdate(uint8_t*, quint32)));

        //connect(worker, SIGNAL(qBusRead(QList<TIniDataLine>*)), this, SLOT(qBusRead(QList<TIniDataLine>*)));
        connect(worker, SIGNAL(qBusRead(quint32*, quint32)), this, SLOT(qBusRead(quint32*, quint32)));

        worker->setUdpAddr(ui->checkBoxUDPSend->isChecked(), ui->lineEditUdpIP->text());

        worker->setSaveToFile(ui->checkBoxEBusFileSave->isChecked());


        ftCtrlThread.start();
        onlineTime.start();
        statusBar->showMessage("connected", 2500);



//        QPalette pal = ui->pushButtonOpenCloseUsb->palette();
//        pal.setColor(QPalette::Button, QColor(Qt::green));
//        ui->pushButtonOpenCloseUsb->setAutoFillBackground(true);
//        ui->pushButtonOpenCloseUsb->setPalette(pal);
//        ui->pushButtonOpenCloseUsb->update();
    }
    else{
        worker->cancel();
        ftCtrlThread.quit();
        ftCtrlThread.wait();
        //delete worker;
        worker = NULL;

        FT_Close(ftHandle);
        ui->pushButtonOpenCloseUsb->setText("open device");
        //onlineTime.stop();
        statusBar->showMessage("disconnected", 2500);

//        QPalette pal = ui->pushButtonOpenCloseUsb->palette();
//        pal.setColor(QPalette::Button, QColor(Qt::red));
//        ui->pushButtonOpenCloseUsb->setAutoFillBackground(true);
//        ui->pushButtonOpenCloseUsb->setPalette(pal);
//        ui->pushButtonOpenCloseUsb->update();

    }
}

void Dialog::uiUpdate(uint8_t *data, quint32 len)
{
    //qDebug("uiUpdate len %d", len);
    setLblColor(ui->labelLockedQbus, data[1]&0x01);
    setLblColor(ui->labelLockedCARU, (data[1]&0x20)==0);
    setLblColor(ui->labelFpga2Present, (data[1]&0x02)==0);

    setLblColor(ui->labelLockedEbus1, data[2]&0x01);
    setLblColor(ui->labelLockedEbus2, data[2]&0x02);
    setLblColor(ui->labelLockedEbus3, data[2]&0x04);
    setLblColor(ui->labelLockedEbus4, data[2]&0x08);
    setLblColor(ui->labelLockedEbus5, data[2]&0x10);
    setLblColor(ui->labelLockedEbus6, data[2]&0x20);
    setLblColor(ui->labelLockedEbus7, data[2]&0x40);
    setLblColor(ui->labelLockedEbus8, data[2]&0x80);

    uint8_t fifoOvfl = data[3];
    setLblColor(ui->labelFifoEbus1, fifoOvfl&0x01);
    setLblColor(ui->labelFifoEbus2, fifoOvfl&0x02);
    setLblColor(ui->labelFifoEbus3, fifoOvfl&0x04);
    setLblColor(ui->labelFifoEbus4, fifoOvfl&0x08);
    setLblColor(ui->labelFifoEbus5, fifoOvfl&0x10);
    setLblColor(ui->labelFifoEbus6, fifoOvfl&0x20);
    setLblColor(ui->labelFifoEbus7, fifoOvfl&0x40);
    setLblColor(ui->labelFifoEbus8, fifoOvfl&0x80);

    fifoOvfl = data[4];
    setLblColor(ui->labelQBusFifoCount, fifoOvfl&0x01);

    setLblColor(ui->labelLockedTIM1, (data[1]&0x04)==0);
    setLblColor(ui->labelLockedTIM2, (data[1]&0x08)==0);

    uint8_t i = 5;
    uint16_t qBusFifoCount = (data[i+0]<<8)|data[i+1];
    ui->labelQBusFifoCount->setText(QString::number(qBusFifoCount));
    ui->labelFifoEbus1->setText(QString::number((data[i+2]<<8)|data[i+3]));
    ui->labelFifoEbus2->setText(QString::number((data[i+4]<<8)|data[i+5]));
    ui->labelFifoEbus3->setText(QString::number((data[i+6]<<8)|data[i+7]));
    ui->labelFifoEbus4->setText(QString::number((data[i+8]<<8)|data[i+9]));
    ui->labelFifoEbus5->setText(QString::number((data[i+10]<<8)|data[i+11]));
    ui->labelFifoEbus6->setText(QString::number((data[i+12]<<8)|data[i+13]));
    ui->labelFifoEbus7->setText(QString::number((data[i+14]<<8)|data[i+15]));
    ui->labelFifoEbus8->setText(QString::number((data[i+16]<<8)|data[i+17]));

    i = 23;
    //qDebug("%x %x %x %x", data[i+0],data[i+1],data[i+2],data[i+3]);
    //qDebug("%x %x %x %x", data[i+4],data[i+5],data[i+6],data[i+7]);
    uint32_t sdram1DataCount = ((data[i+0]&0x3f)<<24)|(data[i+1]<<16)|(data[i+2]<<8)|(data[i+3]);
    uint32_t sdram2DataCount = ((data[i+4]&0x3f)<<24)|(data[i+5]<<16)|(data[i+6]<<8)|(data[i+7]);
    //qDebug() << sdram1DataCount << sdram2DataCount ;
    ui->labelFifoSDRAM1->setText(QString::number(sdram1DataCount));
    ui->labelFifoSDRAM2->setText(QString::number(sdram2DataCount));
    //ui->labelFifoSDRAM3->setText(QString::number((data[i+16]<<8)|data[i+17]));
    //ui->labelFifoSDRAM4->setText(QString::number((data[i+16]<<8)|data[i+17]));
    //qDebug() << QString().sprintf("fifo sdram data %x", data[i+0]) ;
    setLblColor(ui->labelFifoSDRAM1, (data[i+0]>>7) &0x1);
    setLblColor(ui->labelFifoSDRAM2, (data[i+4]>>7) &0x1);
    setLblColor(ui->labelFifoSDRAM3, (data[i+8]>>7) &0x1);
    setLblColor(ui->labelFifoSDRAM4, (data[i+12]>>7) &0x1);

    delete [] data;
    //qDebug("uiHandler");
    if(ui->checkBoxAutoUpr->isChecked() && (qBusFifoCount>0)){
        //on_pushButtonReqQBus_clicked();
        sendReq(0xdd);
    }
    if(ui->checkBoxAutoEBus->isChecked()){
        if(sdram1DataCount > eDlThresh){
            sendReq(0xe0);

        }
        if(sdram2DataCount > eDlThresh){
            sendReq(0xe1);
        }
    }

}





void Dialog::setLblColor(QLabel* l, bool c)
{
    if(c){
        setLblColorRed(l);
    }
    else{
        setLblColorGreen(l);
    }
}
void Dialog::setLblColorRed(QLabel* l)
{
//    QPalette pal = l->palette();
//    //pal.setColor(QPalette::Window, Qt::red);
//    pal.setColor(l->backgroundRole(), Qt::red);
//    l->setPalette(pal);
    l->setStyleSheet("QLabel { background-color : red}");
}

void Dialog::setLblColorGreen(QLabel* l)
{
    //QPalette p = l->palette();
    //p.setColor(l->backgroundRole(), Qt::green);
    //l->setPalette(p);
    l->setStyleSheet("QLabel { background-color : lightgreen}");
}

void Dialog::setLblColorGray(QLabel* l)
{
//    l->setAutoFillBackground(true);
//    QPalette p = l->palette();
//    p.setColor(QPalette::Window, Qt::lightGray);
//    l->setPalette(p);
    l->setStyleSheet("QLabel { background-color : lightGray}");
}

void Dialog::sendReq(uint8_t req)
{
    if(ftHandle == nullptr)
        return;
    uint8_t b[10] = {0xbe, 0xef, req};
//    DWORD written;
//    FT_Write(ftHandle, b, 3, &written);
//    qDebug("written %d", written);

    QVarLengthArray<quint8> arr;
    arr.resize(sizeof(b));
    for(int i=0; i<arr.size(); i++){
        arr[i] = b[i];
    }
    sendQueueMutex.lock();
    sendQueue.enqueue(arr);
    sendQueueMutex.unlock();
}

void Dialog::on_pushButtonReqState_clicked()
{
    sendReq(0xcc);
}

void Dialog::on_pushButtonSendAd9510Ctrl_clicked()
{
    if(ftHandle == nullptr)
        return;

    uint8_t data[] = {0xbe, 0xef, 0xaa, 0x80, 0x4e, 0x00,
                      0xbe, 0xef, 0xaa, 0x00, 0x3c, 0x08,
                      0xbe, 0xef, 0xaa, 0x00, 0x5a, 0x01,

                      0xbe, 0xef, 0xaa, 0x00, 0x49, 0x80, //divider0 bypass
                      0xbe, 0xef, 0xaa, 0x00, 0x4b, 0x80, //divider1 bypass
                      0xbe, 0xef, 0xaa, 0x00, 0x4d, 0x80, //divider2 bypass
                      0xbe, 0xef, 0xaa, 0x00, 0x4f, 0x80, //divider3 bypass
                      0xbe, 0xef, 0xaa, 0x00, 0x51, 0x80, //divider4 bypass
                      0xbe, 0xef, 0xaa, 0x00, 0x53, 0x80, //divider5 bypass
                      0xbe, 0xef, 0xaa, 0x00, 0x55, 0x80, //divider6 bypass
                      0xbe, 0xef, 0xaa, 0x00, 0x57, 0x80, //divider7 bypass
                      0xbe, 0xef, 0xaa, 0x00, 0x5a, 0x01,

                      0xbe, 0xef, 0xaa, 0x00, 0x40, 0x0A, //OUT4 CMOS
                      0xbe, 0xef, 0xaa, 0x00, 0x41, 0x0A, //OUT5 CMOS
                      0xbe, 0xef, 0xaa, 0x00, 0x42, 0x0A, //OUT6 CMOS
                      0xbe, 0xef, 0xaa, 0x00, 0x43, 0x0A, //OUT7 CMOS
                      0xbe, 0xef, 0xaa, 0x00, 0x5a, 0x01,
                      0xbe, 0xef, 0xaa, 0x00, 0x5a, 0x01};

    QVarLengthArray<quint8> arr;
    arr.resize(sizeof(data));
    for(int i=0; i<arr.size(); i++){
        arr[i] = data[i];
    }
    sendQueueMutex.lock();
    sendQueue.enqueue(arr);
    sendQueueMutex.unlock();
    //DWORD written;
    //qDebug("%d", sizeof(data));
    //FT_Write(ftHandle, data, sizeof(data), &written);
    //qDebug("written %d", written);

}

void Dialog::on_pushButtonReqFifoClr_clicked()
{
    sendReq(0x99);
}

uint16_t Dialog::calcCheckSum(uint32_t *arr, uint16_t len)
{
    uint32_t cSum=0;
    for(uint32_t i=0; i<len; i++){
        cSum += (arr[i]&0xffff);
    }
    return ((~cSum)&0xffff);
}

uint32_t Dialog::formPacket(uint32_t *arr, uint16_t len, uint8_t **bToSend)
{
    if((len == 0) || (bToSend == nullptr))
        return 0;
    uint16_t arrlen = len-1;
    arr[0] = (arr[0]&0xff000) | (arrlen & 0xfff);

    uint32_t packetByteLen = (len+1)*3;
    uint32_t arrByteLen = 2+1+2+packetByteLen; //key+mark+len+checksum

    //*bToSend =
    uint8_t *sendArr = new uint8_t[arrByteLen];  //key+mark+len+checksum
    sendArr[0] = 0xbe;
    sendArr[1] = 0xef;
    sendArr[2] = 0xbb;
    sendArr[3] = (packetByteLen>>8)&0xff;
    sendArr[4] = packetByteLen&0xff;
    uint32_t checkSum = calcCheckSum(arr, len)|0x20000;
    for(int i=0; i<len; i++){
        for(int k=0; k<3; k++){
            sendArr[5+i*3 + k] = (arr[i]>>(8*(2-k)))&0xff;
        }
    }
    for(int k=0; k<3; k++){
        sendArr[5+len*3 + k] = (checkSum>>(8*(2-k)))&0xff;
    }

    qDebug("checkSum 0x%x", checkSum);
    *bToSend = sendArr;
    return arrByteLen;
}

void Dialog::on_pushButtonSendReset_clicked()
{
    if(ftHandle == nullptr)
        return;
    uint8_t *bToSend;
    uint32_t packetArr[] = {0x10000};
    uint32_t len = formPacket(packetArr, 1, &bToSend);

    for(int k=0; k<len; k++){
        QString debOut = "send: ";
        int j;
        for(j=0; (j<40) && ((k+j)<len); j++){
            debOut.append(QString("%1 ").arg(bToSend[k+j], 2, 16, QLatin1Char( '0' )));
        }
        k+=j;
        qDebug() << debOut;
    }



    DWORD written;
    FT_Write(ftHandle, bToSend, len, &written);
    qDebug("written %d", written);
    delete [] bToSend;
}

void Dialog::on_pushButtonSendVSK_clicked()
{
    if(ftHandle == nullptr)
        return;
    uint8_t *bToSend;
    uint32_t packetArr[] = {0x150d6,
                            0x38000, 0x20000,   //1.Резерв
                            0x38002, 0x20030,   //2.ВСК слово 1
                            0x38002, 0x20000,  //2.ВСК слово 1
                            0x38004, 0x20000,   //3.ВСК слово 2 (ЧТ)
                            0x38006, 0x20000,   //4.ВСК слово 3 (ЧТ)
                            0x38008, 0x20000,   //5.ВСК слово 4 (ЧТ)
                            0x38030, 0x25e10,   //16. Код К1задержка 1-го строба // [430мкс + t0}
                            0x38032, 0x21f00,   //! 17.Код К1, К2, К7 (ОТКЛ частично)
                            0x38034, 0x28000,   //!!! 18.Код К3, НК
                            0x38036, 0x201ff,   //!!!19.Код К4, К5
                            0x38038, 0x203ff,   //!20.Код К6
                            0x3803a, 0x20007,   //!! 21.Код К8
                            0x3803c, 0x20000,   //22.Шина Е слово управления 1
                            0x3803e, 0x28888,   //23.Шина Е слово управления 2
                            0x38040, 0x2008A};
    uint32_t len = formPacket(packetArr, sizeof(packetArr)/sizeof(uint32_t), &bToSend);

    DWORD written;
    FT_Write(ftHandle, bToSend, len, &written);
    qDebug("written %d", written);
    delete [] bToSend;
}

void Dialog::on_pushButtonSendParams_clicked()
{
    if(ftHandle == nullptr)
        return;
    uint8_t *bToSend;

    uint32_t size = 1000;
    uint32_t *packetArr = new uint32_t[size];
    packetArr[0] = 0x150d6;
    for(int i=1; i<size; i++){
        packetArr[i] = i-1;

    }

    uint32_t len = formPacket(packetArr, size, &bToSend);

    for(int k=0; k<len; k++){
        QString debOut = "recv: ";
        int j;
        for(j=0; (j<40) && ((k+j)<len); j++){
            debOut.append(QString("%1 ").arg(bToSend[k+j], 2, 16, QLatin1Char( '0' )));
        }
        k+=j;
        qDebug() << debOut;
    }

    DWORD written;
    FT_Write(ftHandle, bToSend, len, &written);
    qDebug("written %d", written);
    delete [] bToSend;
    delete [] packetArr;
}

void Dialog::addOpenedFileToReg(QString filePath)
{
    QFile file(filePath);
    if(!file.open(QIODevice::Text|QIODevice::ReadOnly)){
        qDebug("file open error") ;
        return;
    }

    QString openedFileNew, openedIniFiles = settings.value("openedIniFiles", "").toString();
//    openedIniFiles += (openedIniFiles.isEmpty()?"":";") + fileInfo.absoluteFilePath();
//    settings.setValue("openedIniFiles", openedIniFiles);
//    settings.sync();

    bool bPathExist = false;
    QStringList openedIniFilesList = openedIniFiles.split(";");
    foreach(QString s, openedIniFilesList){
        if(s == filePath){
            if(bPathExist)
                continue;
            bPathExist = true;
        }
        openedFileNew += (openedFileNew.isEmpty()?"":";") + s;
    }
    if(bPathExist == false){
        openedFileNew += (openedFileNew.isEmpty()?"":";") + filePath;
    }

    settings.setValue("openedIniFiles", openedFileNew);
    settings.sync();

    file.close();
}

void Dialog::on_pushButtonOpenFile_clicked()
{


    QString filePath =  settings.value("iniFilePath", "./").toString();

    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),
                                                    filePath,
                                                    tr("ini files (*.ini)"));
    qDebug() << fileName ;
    QFile file(fileName);
    if(!file.open(QIODevice::Text|QIODevice::ReadOnly)){
        qDebug("file open error") ;
        return;
    }

    QFileInfo fileInfo(file);
    //qDebug() << fileInfo.absoluteDir().path();
    settings.setValue("iniFilePath", fileInfo.absoluteDir().path());
    settings.sync();

    openIniFile(fileName);
}

void Dialog::openIniFile(QString filePath)
{
    QFile file(filePath);
    if(!file.open(QIODevice::Text|QIODevice::ReadOnly)){
        qDebug("file open error") ;
        return;
    }

    QFileInfo fileInfo(file);

    for(int i=0; i<ui->tabWidgetIniListTabs->count(); i++){
        if(ui->tabWidgetIniListTabs->tabToolTip(i) == filePath){
            statusBar->showMessage("already opened: " + fileInfo.fileName(), 2500);
            return;
        }
    }

    QString contain;
    contain = file.readAll();
    QStringList lines = contain.split("\n");
    TIniDataLine idl;
    QString dataName;
    int32_t dataLinesCount=-1;
    int32_t machineNumber=-1;

    QList<TIniDataLine> dataLineList;
    //dataLineList.clear();


    foreach (QString l, lines) {
        QStringList lineParts = l.split("=");
        if(lineParts.length() != 2)
            continue;
        //qDebug() << l;
        //qDebug() << lineParts[0];
        if(lineParts[0] == "Name"){
            dataName = lineParts[1];
            //qDebug() << "dataName:" << dataName;
            continue;
        }
        if(lineParts[0] == "Count"){
            dataLinesCount = lineParts[1].toUInt();
            //qDebug() << "dataLinesCount:" << dataLinesCount;
            continue;
        }
        if(lineParts[0] == "PM"){
            machineNumber =  lineParts[1].toUInt();
            continue;
        }
        QStringList partsOfLeft = lineParts[0].split("_");
        if(partsOfLeft.length() != 2)
            continue;
        //qDebug() << partsOfLeft[1];
        uint16_t lineListNum = partsOfLeft[1].toUInt();
        if((partsOfLeft[0] == "Address")){
            //qDebug() << qPrintable("addr:") << lineParts[1];
            idl.num = lineListNum;
            idl.addr = (uint32_t)lineParts[1].toInt(nullptr, 16);

        }
        else if((partsOfLeft[0] == "Data") && (idl.num == lineListNum)){
            //qDebug() << qPrintable("data:") << lineParts[1];
            idl.data = lineParts[1].toInt(nullptr, 16);
            //qDebug() << "line "<< QString::number(idl.num) << " addr:" << QString::number(idl.addr, 16) << " data:" << QString::number(idl.data, 16 );

            dataLineList.append(idl);
        }
    }

    dataLineListMap[filePath] = dataLineList;

    QStringList lwStrList;
    foreach(TIniDataLine dll, dataLineList){
        QString listWdgItem;
        listWdgItem.sprintf("%03d:  0x%04x = 0x%04x", dll.num, dll.addr, dll.data);
        //listWdgItem << "line "<< QString::number(idl.num) << " addr:" << QString::number(idl.addr, 16) << " data:" << QString::number(idl.data, 16 );
        lwStrList.append(listWdgItem);
    }

    QString title = dataName;
    if(machineNumber != -1){
        title += "," + QString("PM:%1").arg(machineNumber);
    }
    if(dataLinesCount != -1){
        title += "," +QString("count:%1").arg(dataLinesCount) + " ";
    }
    //ui->listWidgetIniListWidget->addItem(title);
    //ui->listWidgetIniListWidget->addItems(lwStrList);


    //qDebug() << dataLineList.length();
    statusBar->showMessage(QString("file opened: ") + fileInfo.absoluteFilePath(), 2500);

    //int tabInd = ui->tabWidgetIniListTabs->addTab(QWidget(), "test");
    //ui->tabWidgetIniListTabs->setTabsClosable(true);
    QListWidget *lw = new QListWidget();
    lw->addItem(title);
    lw->addItems(lwStrList);
    int tabInd = ui->tabWidgetIniListTabs->addTab(lw, fileInfo.fileName());
    ui->tabWidgetIniListTabs->setTabToolTip(tabInd, fileInfo.absoluteFilePath());


//    QString openedFileNew, openedIniFiles = settings.value("openedIniFiles", "").toString();
//    openedIniFiles += (openedIniFiles.isEmpty()?"":";") + fileInfo.absoluteFilePath();
//    settings.setValue("openedIniFiles", openedIniFiles);
//    settings.sync();
    addOpenedFileToReg(filePath);

}

void Dialog::on_pushButtonWrite_clicked()
{
    int curInd = ui->tabWidgetIniListTabs->currentIndex();
    QString filePath = ui->tabWidgetIniListTabs->tabToolTip(curInd);
    QList<TIniDataLine> dataLineList = dataLineListMap[filePath];

    if(dataLineList.isEmpty())
        return;
    if(ftHandle == nullptr)
        return;
    uint8_t *bToSend;
    uint32_t packetArrLen = 2*dataLineList.size() + 1;
    uint32_t *packetArr = new uint32_t[packetArrLen];
    packetArr[0] = 0x15000;
    for(int i=0; i<dataLineList.size(); i++){
        packetArr[2*i+1] = dataLineList[i].addr|0x30000;
        packetArr[2*i+2] = dataLineList[i].data|0x20000;

    }
    uint32_t len = formPacket(packetArr, packetArrLen, &bToSend);
    delete [] packetArr;
//    DWORD written;
//    FT_Write(ftHandle, bToSend, len, &written);
//    qDebug("len %d, written %d", len,  written);

    QVarLengthArray<quint8> arr;
    arr.resize(len);
    for(int i=0; i<len; i++){
        arr[i] = bToSend[i];
    }
    sendQueueMutex.lock();
    sendQueue.enqueue(arr);
    sendQueueMutex.unlock();


    delete [] bToSend;
}

void Dialog::on_pushButtonRead_clicked()
{
    if(ftHandle == nullptr)
        return;
    uint8_t *bToSend;
    uint32_t packetArr[] = {0x16000,
                            0x38000,    //1.Резерв
                            0x38002,    //2.ВСК слово 1
                            0x38004,    //3.ВСК слово 2 (ЧТ)
                            0x38006,    //4.ВСК слово 3 (ЧТ)
                            0x38008,    //5.ВСК слово 4 (ЧТ)
                            0x38030,    //16. Код К1задержка 1-го строба // [430мкс + t0}
                            0x38032,    //! 17.Код К1, К2, К7 (ОТКЛ частично)
                            0x38034,    //!!! 18.Код К3, НК
                            0x38036,    //!!!19.Код К4, К5
                            0x38038,    //!20.Код К6
                            0x3803a,    //!! 21.Код К8
                            0x3803c,    //22.Шина Е слово управления 1
                            0x3803e,    //23.Шина Е слово управления 2
                            0x38040 };
    uint32_t len = formPacket(packetArr, sizeof(packetArr)/sizeof(uint32_t), &bToSend);
    //delete [] packetArr;
//    DWORD written;
//    FT_Write(ftHandle, bToSend, len, &written);
//    qDebug("packLen %d, len %d, written %d", sizeof(packetArr)/sizeof(uint32_t), len,  written);


    QVarLengthArray<quint8> arr;
    arr.resize(len);
    for(int i=0; i<len; i++){
        arr[i] = bToSend[i];
    }
    sendQueueMutex.lock();
    sendQueue.enqueue(arr);
    sendQueueMutex.unlock();

    delete [] bToSend;

}

//void Dialog::qBusRead(QList<TIniDataLine> *adrDataList)
//{
//    //ui->listWidgetReadedMemory.t


//    delete adrDataList;
//}

void Dialog::qBusRead(uint32_t *arr, uint32_t len)
{
    if(len == 0)
        return;

    TIniDataLine dataLine;
    if((arr[0]&0xff000) == 0x16000){
        QList<TIniDataLine> adrDataList;
        for(int i=1; i<(len-1); i++){
            if( (arr[i]&0xf0000) == 0x30000){
                dataLine.addr = arr[i]&0xffff;
            }
            else if( (arr[i]&0xf0000) == 0x20000){
                dataLine.data = arr[i]&0xffff;
                adrDataList.append(dataLine);
                dataLine.addr=0xffff; dataLine.data=0xffff;
            }
        }
        int rowInd=0;
        ui->listWidgetReadedMemory->insertItem(rowInd++, QString("--- mem read ")+QTime::currentTime().toString("HH:mm:ss") + " ---");
        foreach(TIniDataLine idl, adrDataList){
            QString str;
            //str << "addr:" << QString::number(idl.addr, 16) << " data:" << QString::number(idl.data, 16);
            str.sprintf("0x%04x = 0x%04x", idl.addr, idl.data);
            ui->listWidgetReadedMemory->insertItem(rowInd++, str);
        }
    }
    else if( ((arr[0]&0xff000) == 0x10000) ||
             ((arr[0]&0xff000) == 0x15000) ){
        //qDebug("bl resp");
        int rowInd=0;
        ui->listWidgetReadedMemory->insertItem(rowInd++, QString("--- bl resp ")+QTime::currentTime().toString("HH:mm:ss") + " ---");
        for(int i=0; i<len; i++){
            QString str;
            str.sprintf("0x%05x", arr[i]);
            ui->listWidgetReadedMemory->insertItem(rowInd++, str);
        }

    }


    delete [] arr;
    //emit qBusRead(adrDataList);

}

void Dialog::on_pushButtonClearReadedMem_clicked()
{
    ui->listWidgetReadedMemory->clear();
}

void Dialog::on_checkBoxAutoEBus_clicked(bool checked)
{
    settings.setValue("auto_e", checked);
    settings.sync();
}

void Dialog::on_checkBoxAutoUpr_clicked(bool checked)
{
    settings.setValue("auto_upr", checked);
    settings.sync();
}


void Dialog::on_pushButtonSendRead_clicked()
{
    if(ftHandle == nullptr)
        return;
    const uint32_t testLen = 10000;
    uint8_t *bToSend;
    uint32_t *packetArr = new uint32_t[testLen];
    packetArr[0] = 0x16000;
    for (int i=1; i<testLen;i++) {
        packetArr[i] = 0x38000|(i-1);

    }
    uint32_t len = formPacket(packetArr, testLen, &bToSend);
    delete [] packetArr;
//    DWORD written;
//    FT_Write(ftHandle, bToSend, len, &written);
//    qDebug("packLen %d, len %d, written %d", sizeof(packetArr)/sizeof(uint32_t), len,  written);


    QVarLengthArray<quint8> arr;
    arr.resize(len);
    for(int i=0; i<len; i++){
        arr[i] = bToSend[i];
    }
    sendQueueMutex.lock();
    //for(int i=0; i<10; i++)
        sendQueue.enqueue(arr);
    sendQueueMutex.unlock();

    delete [] bToSend;

}

uint32_t lastMsgCount=0;
uint32_t lastUpdate=0;
void Dialog::updateMsgCounter()
{
    if(worker == nullptr)
        return;
    if(worker->isRunning()){
        int absSecs = onlineTime.elapsed()/1000;
        int secs = absSecs % 60;
        int min = (absSecs / 60) % 60;
        int h = (absSecs / 3600);
        QString timeStr;
        if(h>0)
            timeStr += QString().sprintf("%d:", h);
        if(min>0)
            timeStr += QString().sprintf("%02d:", min);
        timeStr += QString().sprintf("%02d",secs);
        lineEditOnlineCouter->setText(timeStr);
    }
    //lineEditMsgCounter->setText(QString::number(QTime::second()))
    uint32_t updateDelta = QTime::currentTime().msecsSinceStartOfDay() - lastUpdate;
    //if(updateDelta < 1000)
    //    return;
    lastUpdate = QTime::currentTime().msecsSinceStartOfDay();


    uint32_t msgPerSec = worker->getMsgCount();
    //worker->clearMsgCount();
    lineEditMsgCounter->setText(QString::number(worker->getMsgCount()));

}

void Dialog::closeTabReq(int tabInd)
{
    qDebug("req close %d", tabInd);
    QListWidget *lw = (QListWidget*)(ui->tabWidgetIniListTabs->widget(tabInd));
//    delete lw;
    QString filePath = ui->tabWidgetIniListTabs->tabToolTip(tabInd);
    ui->tabWidgetIniListTabs->removeTab(tabInd);
     //ui->tabWidgetIniListTabs->currentWidget()->deleteLater();


    QString openedFileNew, openedIniFiles = settings.value("openedIniFiles", "").toString();
    //openedIniFiles.remove(filePath);
    QStringList openedFileList = openedIniFiles.split(";");
    foreach(QString s, openedFileList){
        if(s == filePath){
            continue;
        }
        openedFileNew += (openedFileNew.isEmpty()?"":";") + s;
    }
    settings.setValue("openedIniFiles", openedFileNew);
    settings.sync();
}



void Dialog::on_lineEditUdpIP_textChanged(const QString &arg1)
{
    qDebug() << "changed " << arg1;
    settings.setValue("udpAddr", arg1);
    settings.sync();
}

void Dialog::on_checkBoxUDPSend_clicked(bool checked)
{
    settings.setValue("sendUdp", checked);
    settings.sync();
    if(worker != nullptr)
        worker->setUdpAddr(checked, ui->lineEditUdpIP->text());
}


void Dialog::on_checkBoxEBusFileSave_clicked(bool checked)
{
    settings.setValue("saveToFile", checked);
    settings.sync();
    if(worker != nullptr)
        worker->setSaveToFile(checked);
}
