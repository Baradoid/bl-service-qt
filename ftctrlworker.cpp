#include "ftctrlworker.h"
#include <QThread>
#include "ftd2xx.h"
#include <QDebug>

#include "dataTypes.h"
#include <QMutex>
#include <QQueue>

#include <QFile>
//FtCtrlWorker::FtCtrlWorker() : QObject(),
//    bSendUdp(false),
//    udpSocketSend(nullptr)
//{

//}

QMutex sendQueueMutex;
QQueue< QVarLengthArray<quint8>> sendQueue;

void FtCtrlWorker::parseStatePacket(uint8_t *data, uint32_t len)
{
    uint8_t *uiData = new uint8_t[len];
    memcpy(uiData, data, len);
    //qDebug("parseStatePacket %x %x %x %x %x %x %x %x", data[0],data[1],data[2],data[3],data[4],data[5],data[6],data[7]);
    emit uiUpdate(uiData, len);
}

void FtCtrlWorker::parseQBusPacket(uint8_t *data, uint32_t len)
{   
    quint32 qBusDataLen = (len)/3;
    quint32 *qBusData = new quint32[qBusDataLen];

    for(int i=0; i<qBusDataLen; i++){
        qBusData[i] = 0;
        for(int k=0; k<3; k++){
            qBusData[i] |= data[i*3+k]<<(8*(2-k));

        }
    }

    for(int i=0; i<qBusDataLen;){
        QString debOut = "qBus:";
        int j;
        for(j=0; (j<40) && ((i+j)<qBusDataLen); j++){
            debOut.append(QString("%1 ").arg(qBusData[i+j], 2, 16));
        }
        i+=j;
        qDebug() << debOut;
    }

    emit qBusRead(qBusData, qBusDataLen);
    //delete [] qBusData;
}

void FtCtrlWorker::parseEBusPacket(uint8_t type, uint8_t *data, uint32_t len)
{
    QFile binFile(QString().sprintf("data_%x.bin", type));
    uint64_t iFileSize = 0;
    if (binFile.open(QIODevice::ReadOnly)){
        iFileSize = binFile.size();
    }

    binFile.close();
    if (!binFile.open(QIODevice::WriteOnly | QIODevice::Append))
        return;
    qDebug() << "fSize: " << iFileSize << "len:" << len ;
    //binFile.resize(binFile.size() + len);
    //binFile.seek(iFileSize);
    binFile.write((char*)data, len);
    //binFile.flush();
    binFile.close();

    QMap<int, QList<uint32_t>> dataMap;

    for(int k=1; k<len;){
        QString debOut = QString().sprintf("recv %x: ",type);
        int j;
        for(j=0; (j<40) && ((k+j)<len); j+=4){
            int i=0;
            int key = data[k+j+i];
            uint32_t d = 0;
            debOut.append(QString("%1").arg(data[k+j+i], 2, 16, QLatin1Char( '0' )));
            for(i=1; (i<4) && ((k+j+i)<len); i++){
                debOut.append(QString("%1").arg(data[k+j+i], 2, 16, QLatin1Char( '0' )));
                d |= data[k+j+i]<<(8*(3-i));
            }
            dataMap[key].append(d);
            debOut.append(" ");
        }
        k+=j;
        //qDebug() << debOut;
    }

    QList<int> keys = dataMap.keys();
    foreach(int key, keys){
        QFile txtFile(QString().sprintf("data_%x.txt", key));
        if (txtFile.open(QIODevice::Append | QIODevice::ReadWrite)) {
            QTextStream stream(&txtFile);

            QList<uint32_t> valueArr = dataMap[key];
            foreach(uint32_t val, valueArr){
                stream << QString("%1").arg(val, 2, 16, QLatin1Char( '0' )) << endl;
            }
            txtFile.close();

        }
    }



    if(bSendUdp)
        sendUdpDgm((char*)data, len);
}

typedef enum{
    searchForKeyBe,
    searchForKeyEf,
    searchForLen1,
    searchForLen2,
    searchForLen3,
    searchForLen4,
    searchForPacketMark,
    copyData
} TParserState;

TParserState state = searchForKeyBe;
uint32_t packLen=0;
uint8_t packMark=0;
uint8_t  *packData = NULL;
uint32_t packIdx=0;
void FtCtrlWorker::parseData(uint8_t *buf, uint32_t len)
{
    msgCount++;
//    for(int i=0; i<len;){
//        QString debOut = "recv: ";
//        int j;
//        for(j=0; (j<40) && ((i+j)<len); j++){
//            debOut.append(QString("%1 ").arg(buf[i+j], 2, 16, QLatin1Char( '0' )));
//        }
//        i+=j;
//        qDebug() << debOut;
//    }
    //qDebug("parse data %d", state);
    for(int i=0; i<len; i++){
        if( (state == searchForKeyBe) && (buf[i]==0xbe)){
            state = searchForKeyEf;
        }
        else if ( (state == searchForKeyEf) && (buf[i]==0xef)){
            state = searchForLen1;
        }
        else if (state == searchForLen1){
            packLen = (buf[i]<<24);
            state = searchForLen2;
        }
        else if (state == searchForLen2){
            packLen |= (buf[i]<<16);
            state = searchForLen3;
        }
        else if (state == searchForLen3){
            packLen |= (buf[i]<<8);
            state = searchForLen4;
        }
        else if (state == searchForLen4){
            packLen |= buf[i];
            //state = searchForPacketMark;
            state = copyData;
            //qDebug("packLen: %d", packLen);
            packData = new uint8_t[packLen];
            if(packData == nullptr){
                qDebug("!!!! copyData: error create pack data !!!!");
            }
            packIdx = 0;
        }
        else if (state == copyData){
            //packMark = buf[i];

            uint32_t bytesToCopy = qMin(len-i, packLen-packIdx);
            //qDebug("copyData: packLen %d, packIdx %d, bytesToCopy %d", packLen, packIdx, bytesToCopy);
            memcpy(packData+packIdx, buf+i, bytesToCopy);

            if((packLen-packIdx) <= (len-i)){
                state = searchForKeyBe;
                packIdx = 0;
                if(packData[0] == 0xcc){
                    parseStatePacket(&(packData[0]), packLen);
                }
                else if(packData[0] == 0xbb){
                    parseQBusPacket(&(packData[1]), packLen-1);
                }
                else if((packData[0]&0xf0) == 0xe0){
                    qDebug("recv packet %x", packData[0]);
                    //parseQBusPacket(&(packData[1]), packLen-1);
                    parseEBusPacket(packData[0]&0xff, &(packData[0]), packLen);
                }
//                else{
//                    qDebug("==== recv unknown packet =======");
////                    for(int k=0; k<packLen; k++){
////                        QString debOut = "recv: ";
////                        int j;
////                        for(j=0; (j<20) && ((k+j)<packLen); j++){
////                            debOut.append(QString("%1 ").arg(packData[k+j], 2, 16, QLatin1Char( '0' )));
////                        }
////                        k+=j;
////                        qDebug() << debOut;
////                    }

//                }
                delete [] packData;

            }
            else
                packIdx += bytesToCopy;
            i += bytesToCopy;
        }
    }
}

void FtCtrlWorker::process()
{
    qDebug("thread started");
    FT_STATUS ftStatus;
    HANDLE hEvent;
    DWORD EventMask;

    hEvent = CreateEvent(NULL, false, false, L"");
    EventMask = FT_EVENT_RXCHAR;
    ftStatus = FT_SetEventNotification(h, EventMask, hEvent);

    while(!isCancelled){
        if(WaitForSingleObject(hEvent, 50) == WAIT_TIMEOUT){
            sendQueueMutex.lock();
            DWORD written;
            if(sendQueue.isEmpty()){
                uint8_t b[10] = {0xbe, 0xef, 0xcc};
                FT_Write(h, b, 3, &written);
            }
            else{
                QVarLengthArray<quint8> sendArr  = sendQueue.dequeue();
                quint8 *d = sendArr.data();
                FT_Write(h, d, (DWORD)sendArr.length(), &written);
            }
            //qDebug("written %d", written);
            sendQueueMutex.unlock();


            continue;
        }
        DWORD RxBytes, TxBytes, EventDWord, bytesRecvd;
        FT_GetStatus(h, &RxBytes, &TxBytes, &EventDWord);
        //qDebug("rx: %d, tx %d", RxBytes, TxBytes);
        uint8_t *buf = new uint8_t[RxBytes];
        if(buf == nullptr)
            qDebug("error create buf");

        //emit
        ftStatus = FT_Read(h, buf, RxBytes, &bytesRecvd);
        //qDebug("rxBytes %d, FT_Read %d, recvd %d", RxBytes, ftStatus, bytesRecvd);
//        for(quint32 k=0; k<bytesRecvd;){
//            QString debOut = "recv: ";
//            quint32 j;
//            for(j=0; (j<40) && ((k+j)<bytesRecvd); j++){
//                debOut.append(QString("%1 ").arg(buf[k+j], 2, 16, QLatin1Char( '0' )));
//            }
//            k+=j;
//            qDebug() << debOut;
//        }

        parseData(buf, bytesRecvd);
        //for(int i=0; i<bytesRecvd; i++){
        //}
        delete [] buf;
    }

//    QString result;
//    /* ... here is the expensive or blocking operation ... */
//    emit resultReady(result);
//    while(true){

//        qDebug("ping");
//        QThread::sleep(500);
//    }
}

void FtCtrlWorker::setUdpAddr(bool bSendUDP, QString addr)
{
    bSendUdp = bSendUDP;
    udpAddr = addr;

}

void FtCtrlWorker::sendUdpDgm(char *d, uint64_t len)
{
    if(udpSocketSend == nullptr){
        udpSocketSend = new QUdpSocket(this);
        udpSocketSend->bind(udpSocketSend->localPort());
    }

    QHostAddress ha(udpAddr);
    int ret = udpSocketSend->writeDatagram((char*)d, (quint64)len, ha, 61557);
    qDebug() << "udp send ret:" << ret;
}


void FtCtrlWorker::setSaveToFile(bool save)
{
    bSaveToFile = save;
}
