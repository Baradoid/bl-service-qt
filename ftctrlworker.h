#ifndef FTCTRLWORKER_H
#define FTCTRLWORKER_H

#include <QObject>
#include <QThread>
#include <QUdpSocket>
#include "ftd2xx.h"
#include "dataTypes.h"

class FtCtrlWorker : public  QObject
{
    Q_OBJECT

public slots:
    //void doWork(const QString &parameter);
    void process();
public:
    //FtCtrlWorker();
//{
//        bSendUdp = false;
//        udpSocketSend = nullptr;
//    }
    void clearMsgCount(){ msgCount=0;}
    uint32_t getMsgCount(){ return msgCount; }

    void setHandle(FT_HANDLE _h) {h = _h;};
    //explicit FtCtrlWorker(FT_HANDLE h);
    void cancel(){isCancelled = true;};

    bool isRunning(){return !isCancelled;};
    void setUdpAddr(bool, QString addr);
    void setSaveToFile(bool);

signals:
    void resultReady(const QString &result);
//public slots:
//    void doWork(const QString &parameter);
    void uiUpdate(uint8_t *data, quint32 len);
    //void qBusRead(QList<TIniDataLine> *adrDataList);
    void qBusRead(quint32 *arr, quint32 len);



private:
    FT_HANDLE h;
    bool isCancelled;
    void parseData(uint8_t *buf, uint32_t len);
    void parseStatePacket(uint8_t *data, uint32_t len);
    void parseQBusPacket(uint8_t *data, uint32_t len);
    void parseEBusPacket(uint8_t type, uint8_t *data, uint32_t len);

    bool bSendUdp;
    QString udpAddr;
    QUdpSocket *udpSocketSend;
    void sendUdpDgm(char *d, uint64_t len);
    uint32_t msgCount;
    bool bSaveToFile;




};

#endif // FTCTRLWORKER_H
