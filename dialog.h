#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include "ftd2xx.h"
#include <QThread>
#include "ftctrlworker.h"
#include <QLabel>
#include <QSettings>
#include <QLineEdit>
#include <QTime>
#include <QStatusBar>

QT_BEGIN_NAMESPACE
namespace Ui { class Dialog; }
QT_END_NAMESPACE

class Dialog : public QDialog
{
    Q_OBJECT

    QThread ftCtrlThread;
public:
    Dialog(QWidget *parent = nullptr);
    ~Dialog();

private slots:
    void on_pushButtonOpenCloseUsb_clicked(bool checked);

    void on_pushButtonReqState_clicked();
    void uiUpdate(uint8_t *data, quint32 len);

    void on_pushButtonSendAd9510Ctrl_clicked();

    void on_pushButtonReqFifoClr_clicked();

    void on_pushButtonSendReset_clicked();

    //void on_pushButtonReqQBus_clicked();

    void on_pushButtonSendVSK_clicked();

    void on_pushButtonSendParams_clicked();

    void on_pushButtonOpenFile_clicked();

    void on_pushButtonWrite_clicked();

    void on_pushButtonRead_clicked();
    //void qBusRead(QList<TIniDataLine> *adrDataList);
    void qBusRead(quint32 *arr, quint32 len);

    void on_pushButtonClearReadedMem_clicked();

    void on_pushButtonSendRead_clicked();
    void closeTabReq(int);

    void on_checkBoxAutoEBus_clicked(bool checked);

    void on_checkBoxAutoUpr_clicked(bool checked);

    void on_lineEditUdpIP_textChanged(const QString &arg1);

    void on_checkBoxUDPSend_clicked(bool checked);

    void on_checkBoxEBusFileSave_clicked(bool checked);

private:
    Ui::Dialog *ui;

    FT_HANDLE ftHandle;

    void sendReq(uint8_t);
    FtCtrlWorker *worker;
    void setLblColor(QLabel*, bool);
    void setLblColorRed(QLabel*);
    void setLblColorGreen(QLabel*);
    void setLblColorGray(QLabel*);

    uint16_t calcCheckSum(uint32_t*,uint16_t);
    uint32_t formPacket(uint32_t*,uint16_t,uint8_t**);



    QSettings settings;
    QLineEdit *lineEditMsgCounter;
    QLineEdit *lineEditOnlineCouter;

    void updateMsgCounter();
    uint32_t msgs;
    QTime onlineTime;
    QStatusBar *statusBar;

    void openIniFile(QString);
    void addOpenedFileToReg(QString);

    QMap<QString, QList<TIniDataLine>> dataLineListMap;


    uint32_t eDlThresh;

signals:
    void operate(const QString &);
};
#endif // DIALOG_H
